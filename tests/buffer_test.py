from pymongo import MongoClient
import random , datetime , time


def generate_data():
    result = {
			'i1': None,
			'i2': None,
			'i3': None,
			'u1': None,
			'u2': None,
			'u3': None,
			'p': None,
			'q': None,
			'f': None,
			'fromDate': None,
			'deviceId': None
		}
    result['i1']      = 	round(random.uniform(1,3), 1)
    result['i2']      =	    round(random.uniform(1,3), 1)
    result['i3']      = 	round(random.uniform(1,3), 1)
    result['u1']      =	    round(random.uniform(220,230), 1)
    result['u2']      =	    round(random.uniform(220,230), 1)
    result['u3']      =	    round(random.uniform(220,230), 1)
    result['p']       =	    round(random.uniform(10,15), 1)
    result['q']       =	round(random.uniform(10,15), 1)
    result['f']       =	50.0
    result['fromDate']     =  datetime.datetime.now().replace(microsecond=0).isoformat()+ "Z"

    return result

def connect_to_db():
    client = MongoClient('mongodb://localhost:27017/db')
    db = client.get_default_database()
    return db['buffer']

'''
This test sets the maximum buffer size. Fills the buffer with data
points by using the Buffer module and checks if the oldest data is removed in favour of the newest one added.
Second part of this test checks if before getting all the data the buffer has not exceeded its max size and
that the buffer is cleared after data is extracted from it.
'''
def buffer_test(buffer):
        test_status = False
        max_buffer_size = 4
        #Setup checker connection to mongodb
        database = connect_to_db()

        buffer_handler = buffer(buffersize=max_buffer_size)

        #Test 1
        #Clear buffer
        print 'Starting Shift Buffer test.\n'
        database.remove({})

        #generate test data
        test_data = []
        for index, data in enumerate(range(max_buffer_size + 3)):
            test_data.append(generate_data())
            print str(index+1) + ' data point added to test_data'
            time.sleep(1)
        #Add generated data points with the tested Buffer module and start checking oldest data points
        oldest_data_point = 0
        for index, data in enumerate(test_data):
            buffer_handler.add_to_buffer(data)
            if index+1 > max_buffer_size:
                oldest_data_point += 1
                data_in_buffer = list(database.find().sort("fromDate", 1)) # return data in ascending order
                #compare data_points
                if cmp(data_in_buffer[0],test_data[oldest_data_point]) == 0:
                    test_status = True
                    print 'Oldest data in buffer sucessfuly shifted!'
                else:
                    test_status = False
                    print 'Error: Oldest data in buffer is wrong'

        if test_status == True:
            print 'Shift Buffer test PASSED!\n'
        else:
            print 'Shift Buffer test FAILED!\n'

        test_status = False

        print 'Starting Buffer extract test.\n'
        buffer_contents = list(database.find())
        if len(buffer_contents) <= max_buffer_size:
            test_status = True
            print 'Buffer has not exceeded its size.'
        else:
            test_status = False
            print 'Buffer has exceeded its size.'

        buffer_handler.get_data_from_buffer()
        buffer_contents_size = len(list(database.find()))
        if buffer_contents_size > 0:
            test_status = False
            print 'Buffer was not emptied after data extraction.'
        else:
            test_status = True
            print 'Buffer was emptied after data extraction.'

        if test_status == True:
            print 'Shift Buffer extract test PASSED!\n'
        else:
            print 'Shift Buffer extract test FAILED!\n'


        database.remove({})
