import requests ,jsonpickle


class Client:

    def __init__(self,token,server,deviceid):
        self.token = token
        self.server = server
        self.deviceid = deviceid

    def post_current_value(self,device_data):
        try:
            request = requests.post(self.server + "/device/" + self.deviceid + "/data/current",
                                    data=jsonpickle.encode(device_data),
                                    headers={"Content-type": "application/json", "Authorization": self.token})
            status_code = int(request.status_code)
            if status_code < 200 or status_code >= 300:
                print "Server returned " + str(status_code) + " response"
            else:
                print str(request)
                return {"status":'ok'}

        except requests.exceptions.RequestException as e:
            #print e[0][0] # This is used to take the first argument from the response.
            return {"status":'error',"type":e[0][0]}

    def post_average_value(self,device_data):
        try:
            request = requests.post(self.server + "/device/" + self.deviceid + "/data/average",
                                    data=jsonpickle.encode(device_data),
                                    headers={"Content-type": "application/json", "Authorization": self.token})

            status_code = int(request.status_code)
            if status_code < 200 or status_code >= 300:
                print "Server returned " + str(status_code) + " response"
            else:
                print str(request) + "avg"
                return {"status":'ok'}
        except requests.exceptions.RequestException as e:
            return {"status":'error',"type":e[0][0]}

    def post_bulk_average_value(self,device_data_list):
        try:
            request = requests.post(self.server + "/device/" + self.deviceid + "/data/average/bulk",
                                    data=jsonpickle.encode(device_data_list),
                                    headers={"Content-type": "application/json", "Authorization": self.token})
            status_code = int(request.status_code)
            if status_code < 200 or status_code >= 300:
                print "Server returned " + str(status_code) + " response"

            return {"status":'ok'}
        except requests.exceptions.RequestException as e:
            return {"status":'error',"type":e[0][0]}
