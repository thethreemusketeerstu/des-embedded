from pymongo import MongoClient

class Buffer:

    def __init__(self,buffersize):
        self.size = buffersize
        self.client = MongoClient('mongodb://localhost:27017/db')
        self.db = self.client.get_default_database()
        self.buffer = self.db['buffer']

    def add_to_buffer(self,data):
        if int(self.size) == self.buffer.count():
            bufferItems = list(self.buffer.find().sort("fromDate", 1)) # return data in ascending order
            self.buffer.remove({'_id':bufferItems[0]['_id']})
            return self.buffer.insert_one(data)
        else:
            return self.buffer.insert_one(data)

    def check_buffer_size(self):
        return self.buffer.count()

    def get_data_from_buffer(self):
        result = self.buffer.find()
        array = []
        for data in result:
            array.append(data)
        self.buffer.remove({})
        return array
