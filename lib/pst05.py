'''
This class presents the functunality to read data from PST05 energy meter sensor
'''
import random , time , datetime

class pst05:

	def __init__(self,debug=False,deviceId=''):
		self.debug = debug
		self.deviceId = deviceId
		self.avg_i1=0
		self.avg_i2=0
		self.avg_i3=0
		self.avg_u1=0
		self.avg_u2=0
		self.avg_u3=0
		self.avg_p=0
		self.avg_q=0
		self.avg_f=0
		self.latest_time=''
		self.measureCounter=0
		self.result = {
			'i1': None,
			'i2': None,
			'i3': None,
			'u1': None,
			'u2': None,
			'u3': None,
			'p': None,
			'q': None,
			'f': None,
			'fromDate': None,
			'deviceId': None
		}


	def readData(self):
		if(self.debug == True):
			self.result['i1']      = 	round(random.uniform(1,3), 1)
			self.result['i2']      =	round(random.uniform(1,3), 1)
			self.result['i3']      = 	round(random.uniform(1,3), 1)
			self.result['u1']      =	round(random.uniform(220,230), 1)
			self.result['u2']      =	round(random.uniform(220,230), 1)
			self.result['u3']      =	round(random.uniform(220,230), 1)
			self.result['p']   =	round(random.uniform(10,15), 1)
			self.result['q'] =	round(random.uniform(10,15), 1)
			self.result['f']     =	50.0
			self.result['fromDate']     =  datetime.datetime.now().replace(microsecond=0).isoformat()+ "Z"
			self.result['deviceId'] = self.deviceId

			self.avg_i1=self.avg_i1 + self.result['i1']
			self.avg_i2=self.avg_i2 + self.result['i2']
			self.avg_i3=self.avg_i3 + self.result['i3']
			self.avg_u1=self.avg_u1 + self.result['u1']
			self.avg_u2=self.avg_u2 + self.result['u2']
			self.avg_u3=self.avg_u3 + self.result['u3']
			self.avg_p=self.avg_p + self.result['p']
			self.avg_q=self.avg_q + self.result['q']
			self.avg_f=self.avg_f + self.result['f']
			self.latest_time=self.result['fromDate']

			self.measureCounter=self.measureCounter+1
			return self.result
		else:
			print 'Non debug version of readData is still not implemented'
			return False

	def getAvarageData(self):
		result = {
			'i1': self.avg_i1/self.measureCounter,
			'i2': self.avg_i2/self.measureCounter,
			'i3': self.avg_i3/self.measureCounter,
			'u1': self.avg_u1/self.measureCounter,
			'u2': self.avg_u2/self.measureCounter,
			'u3': self.avg_u3/self.measureCounter,
			'p': self.avg_p/self.measureCounter,
			'q': self.avg_q/self.measureCounter,
			'f': self.avg_f/self.measureCounter,
			'fromDate': self.latest_time,
			'deviceId': self.deviceId
		}
		self.avg_i1 = self.avg_i2 = self.avg_i3 = 0
		self.avg_u1 = self.avg_u2 = self.avg_u3 = 0
		self.avg_p = self.avg_q = self.avg_f = 0
		return result
