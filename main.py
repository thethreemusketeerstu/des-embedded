import argparse , time , logging
from ConfigParser import SafeConfigParser
from lib.pst05 import pst05
from lib.client import Client
from lib.buffer import Buffer

def main(args):
	avarageCounter = 0 # When this counter reaches the avarageCount value we send the avarage of all measurements.
	#Set poll interval for sensor reads
	pollInterval = int(args['pollinterval'])
	logger = setupLoger(args['loglevel'])
	client = Client(server=args['server'],deviceid=args['deviceid'],token=args['token'])
	buffer = Buffer(buffersize=args['buffersize'])
	pst05_sensor = pst05(debug=bool(args['debug']),deviceId=args['deviceid'])

	while(True):
		#Make a measurement and send it as latest data
		data = pst05_sensor.readData()
		status = client.post_current_value(data)

		# Current value login
		if status['status'] is 'error':
			print status['type']
		#Prepair for avaraging the measurements
		avarageCounter = avarageCounter + 1


		# Avarage and buffer logic
		#Check the configured avarage counter
		#If max value reached send avarage data
		if avarageCounter is int(args['avaragecount']):
			avg_data = pst05_sensor.getAvarageData()
			avg_status = client.post_average_value(avg_data)
			#If server is out of reach save the data to a buffer/file
			if avg_status['status'] is 'error':
				print avg_status['type']
				buffer.add_to_buffer(avg_data)
				logging.debug('append to buffer')
			#If there is connection to the server and there is data in the buffer/File
			#Send the saved data and clear the buffer/file
			else:
				if buffer.check_buffer_size() > 0:
					# get_data_from_buffer clears the whole buffer
					client.post_bulk_average_value(buffer.get_data_from_buffer())

			#clear the counter and prepair for new cycle of saving data before sending avarage to server
			avarageCounter = 0


		time.sleep(pollInterval)



def parseArgs():
	#Parsing input arguments
	parser = argparse.ArgumentParser(description='This application reads energy measurement data form PST05 sensor and sends it to a remote server.')
  	parser.add_argument('--config', type=str, required=False, default='conf/conf.ini', help='String containig location to conf file')
	args = parser.parse_args()

	#Open conf.ini file and parse configs
	conf = SafeConfigParser()
	conf.read(args.config)
	configDict = {}
	options = conf.options('default')
	for option in options:
		configDict[option] = conf.get('default',option)

	return configDict

def setupLoger(level):
	#Setup logger
	logger = logging.getLogger("App")
	# Create a file handler
	handler = logging.FileHandler('logs/runtime.log')
	if level == 'DEBUG':
		logger.setLevel(logging.DEBUG)
		handler.setLevel(logging.DEBUG)
	elif level == 'INFO':
		logger.setLevel(logging.INFO)
		handler.setLevel(logging.INFO)
	elif level == 'WARN':
		logger.setLevel(logging.WARN)
		handler.setLevel(logging.WARN)
	else:
		logger.setLevel(logging.ERROR)
		handler.setLevel(logging.ERROR)

	# create a logging format
	formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
	handler.setFormatter(formatter)

	# add the handlers to the logger
	logger.addHandler(handler)

	return logger



if __name__ == "__main__":
	args = parseArgs()
	main(args)
