import argparse , os
from lib.buffer import Buffer
from tests.buffer_test import buffer_test

def parseArgs():
	#Parsing input import argparsearguments
	parser = argparse.ArgumentParser(description='This is a test suite for des-embedded application.')
  	parser.add_argument('--test', type=str, required=True, help='Type the name of the test you want to run.')
	return parser.parse_args()


def main(args):
    for test in os.listdir('./tests'):
        if 'pyc' not in test and '__init__' not in test:
            if args.test in test:
                run_test(args.test)
            else:
                print "No test found!"


def run_test(test):
    if str(test) == 'buffer_test':
        buffer_test(buffer=Buffer)
    else:
        print 'Can\'t run test'

if __name__ == "__main__":
	args = parseArgs()
	main(args)
