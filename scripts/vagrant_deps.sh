#!/bin/bash

# Installing virtual environment tool
sudo apt-get update
sudo apt-get install -y python-pip

# Create virtualenv
cd /vagrant
export LC_ALL=C

sudo pip install -r requirements.txt

# MongoDB
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
echo "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list
sudo apt-get update
sudo apt-get install -y mongodb-org
