#!/bin/bash

# Installing virtual environment tool
sudo apt-get install -y python-pip
sudo pip install virtualenv
# Show version
echo "Virtualenv version:"
virtualenv --version

# Create virtualenv
cd /vagrant
echo "Creating virtualenv for des-embedded"
virtualenv env
