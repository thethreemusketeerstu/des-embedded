# DES-EMBEDDED

This repository contains an application that will work on an embedded system (Raspberry Pi 3) and will read sensor data form PST05 power meter. The data will be sent to a remote server.

## Getting Started



### Prerequisites

If you use it on Raspberry Pi 3:

First you will need virtualenv with pip and python 2.7 installed.
You may have to make the install_deps.sh script executable before calling it.

```sh
./scripts/install_deps.sh
```

and after that activate your virtual env

```sh
source env/bin/activate
```

If you use it with Vagrant:
```sh
vagrant up
```
Before running source script/fixmongo

To run tests use:
python run_test.py --test <test_name>


### Installing

Before running the application you need to install the dependant pip modules

```sh
pip install -r requirements.txt
```
## Initial project requrements and implementation
### Features
1. Reads data with measurements (i1,i2,i3,u1,u2,u3,p,q,f) from PST05

	1.1 PST05 emulation with arduino and URART - NOT IMPLEMENTED

	Need to write a scketch for arduino and test uart capabilities of Raspberry Pi 3.

	1.2 Debug capability and generation of random data for measurements - IMPLEMENTED

	The script generates data for the measurements and forwards them to backend client.

2. Sends measurement data to backend server

	2.1 Send latest data measurement - IMPLEMENTED

	2.2 Send avaraged data for 15 minutes - IMPLEMENTED (configurable)


3. Data buffer

	3.1 Save data for at least 24 hours in case of no connection to backend server - IMPLEMENTED (configurable)

	The buffer is implemented with MongoDB. Need to add capability for tracing and not exceeding buffer size.

## Config file usage: conf.ini

1. debug - [True/False] For now it is always true. This states if data is generated or taken from PST05.

2. pollinterval - The time in seconds between every read/generation of data form PST05.

3. avarageCount - How many readings need to be done before sending avarege data to backend server.

4. buffersize - The maximum size of avarage data measurements that will be saved in buffer(MongoDB).

5. loglevel - [DEBUG/WARN/ERROR/INFO] .... desn't work well.

6. deviceid - ID of the device.

7. token - Token for authentication 

8. server - server protocol , address and port (<http/https>://<address>:<port>)

## Running buffer test:

```sh
python run_test.py --test buffer_test
```